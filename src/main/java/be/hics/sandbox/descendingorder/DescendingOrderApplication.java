package be.hics.sandbox.descendingorder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DescendingOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(DescendingOrderApplication.class, args);

        System.out.println(DescendingOrder.sortDesc(54897410));
        System.out.println(DescendingOrder.sortDesc(1458752));
        System.out.println(DescendingOrder.sortDesc(521984));
        System.out.println(DescendingOrder.sortDesc(4698716));
        System.out.println(DescendingOrder.sortDesc(9948254));
    }
}
